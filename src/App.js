import React, { Component } from 'react'
import { ThemeProvider } from 'emotion-theming'

import theme from './styles/theme'
import CrossCountryPlanner from './CrossCountryPlanner'

class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <CrossCountryPlanner />
      </ThemeProvider>
    )
  }
}

export default App
