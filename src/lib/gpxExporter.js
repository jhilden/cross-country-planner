import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import ReactDOMServer from 'react-dom/server'

class GpxExporter extends PureComponent {
  static propTypes = {
    waypoints: PropTypes.array.isRequired
  }

  render() {
    const { waypoints } = this.props
    return (
      <gpx version="1.1" creator="Cross Country Planner">
        <rte>
          <name>Exported Route</name>
          {waypoints.map((waypoint, index) => (
            <rtept
              key={index}
              lat={waypoint.coordinates.x}
              lon={waypoint.coordinates.y}
            >
              <name>{`Waypoint ${index + 1}`}</name>
            </rtept>
          ))}
        </rte>
      </gpx>
    )
  }
}

function gpxDataUrlFor(waypoints) {
  const xmlBody = ReactDOMServer.renderToStaticMarkup(
    <GpxExporter waypoints={waypoints} />
  )
  const format = 'text/xml'
  const charset = 'utf-8'
  const xmlVersion = '<?xml version="1.0" encoding="UTF-8" standalone="no" ?>'
  return `data:${format};charset=${charset},${xmlVersion}${xmlBody}`
}

export default gpxDataUrlFor
