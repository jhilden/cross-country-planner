import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled/macro'
import uniqid from 'uniqid'

import SideBar from './sidebar/SideBar'
import Map from './map/Map.js'

const SIDEBAR_WIDTH = 300
const MAP_WIDTH = 958
const PLANNER_HEIGHT = 719

class CrossCountryPlanner extends PureComponent {
  static propTypes = {
    waypoints: PropTypes.array
  }

  static defaultProps = {
    waypoints: []
  }

  constructor(props) {
    super(props)
    this.state = {
      waypoints: props.waypoints
    }
  }

  handleDeleteWaypoint = id => {
    const waypoints = [...this.state.waypoints]
    const index = waypoints.findIndex(wp => wp.id === id)
    waypoints.splice(index, 1)
    this.setState({ waypoints })
  }

  handleAddWaypoint = (pageX, pageY) => {
    const x = pageX - SIDEBAR_WIDTH
    const y = pageY
    const waypoints = [...this.state.waypoints]
    waypoints.push({ id: uniqid(), coordinates: { x, y } })
    this.setState({ waypoints })
  }

  handleMoveWaypoint = (id, x, y) => {
    const waypoints = [...this.state.waypoints]
    const index = waypoints.findIndex(wp => wp.id === id)
    waypoints[index].coordinates = { x, y }
    this.setState({ waypoints })
  }

  handleChangeWaypointOrder = (sourceIndex, targetIndex) => {
    const waypoints = [...this.state.waypoints]
    waypoints.splice(targetIndex, 0, waypoints.splice(sourceIndex, 1)[0])
    this.setState({ waypoints })
  }

  render() {
    const { waypoints } = this.state
    return (
      <Wrapper>
        <SideBarContainer>
          <SideBar
            waypoints={waypoints}
            onWaypointDelete={this.handleDeleteWaypoint}
            onWaypointChangeOrder={this.handleChangeWaypointOrder}
          />
        </SideBarContainer>
        <Map
          waypoints={waypoints}
          onWaypointMove={this.handleMoveWaypoint}
          onWaypointAdd={this.handleAddWaypoint}
          width={MAP_WIDTH}
          height={PLANNER_HEIGHT}
        />
      </Wrapper>
    )
  }
}

export default CrossCountryPlanner

const Wrapper = styled.div({
  display: 'flex',
  flexDirection: 'row'
})

const SideBarContainer = styled.div({
  width: SIDEBAR_WIDTH,
  height: PLANNER_HEIGHT
})
