import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled/macro'

import WaypointCircle from './WaypointCircle'
import WaypointsEdge from './WaypointsEdge'
import middleEarthMap from '../assets/middle-earth-map.jpg'

class Map extends PureComponent {
  static propTypes = {
    waypoints: PropTypes.array,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    onWaypointMove: PropTypes.func.isRequired,
    onWaypointAdd: PropTypes.func.isRequired
  }

  edges() {
    const { waypoints } = this.props
    let edges = []
    waypoints.forEach((waypoint, index) => {
      if (index > 0) {
        const prevWaypoint = waypoints[index - 1]
        edges.push({
          x1: prevWaypoint.coordinates.x,
          y1: prevWaypoint.coordinates.y,
          x2: waypoint.coordinates.x,
          y2: waypoint.coordinates.y
        })
      }
    })
    return edges
  }

  handleMapClick = event => {
    const { onWaypointAdd } = this.props
    const { pageX, pageY } = event
    onWaypointAdd(pageX, pageY)
  }

  render() {
    const { waypoints, width, height, onWaypointMove } = this.props
    return (
      <Background width={width} height={height}>
        <MapCanvas width={width} height={height} onClick={this.handleMapClick}>
          {this.edges().map((edge, index) => (
            <WaypointsEdge
              key={`${edge.x1}-${edge.y1}-${edge.x2}-${edge.y2}`}
              {...edge}
            />
          ))}
          {waypoints.map((waypoint, index) => (
            <WaypointCircle
              key={waypoint.id}
              index={index}
              id={waypoint.id}
              x={waypoint.coordinates.x}
              y={waypoint.coordinates.y}
              onMove={onWaypointMove}
            />
          ))}
        </MapCanvas>
      </Background>
    )
  }
}

export default Map

const Background = styled.div(props => ({
  width: props.width,
  height: props.height,
  backgroundImage: `url(${middleEarthMap})`
}))

const MapCanvas = styled.svg(props => ({
  cursor: 'cell'
}))
