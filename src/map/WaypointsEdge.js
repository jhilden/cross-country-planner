import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled/macro'

class WaypointsEdge extends PureComponent {
  static propTypes = {
    x1: PropTypes.number.isRequired,
    y1: PropTypes.number.isRequired,
    x2: PropTypes.number.isRequired,
    y2: PropTypes.number.isRequired
  }

  render() {
    return <Line {...this.props} />
  }
}

export default WaypointsEdge

const Line = styled.line(props => ({
  stroke: props.theme.blue,
  strokeWidth: 8
}))
