import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled/macro'

const CIRCLE_RADIUS = 16

class WaypointCircle extends PureComponent {
  static propTypes = {
    index: PropTypes.number.isRequired,
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    onMove: PropTypes.func.isRequired
  }

  state = {
    isMoving: false
  }

  handleMouseDown = event => {
    const { x, y } = this.props
    this.moving = {
      startX: x,
      startY: y,
      pageX: event.pageX,
      pageY: event.pageY
    }
    this.setState({ isMoving: true })
    document.addEventListener('mousemove', this.handleMouseMove)
  }

  handleMouseUp = () => {
    document.removeEventListener('mousemove', this.handleMouseMove)
    this.moving = {}
    this.setState({ isMoving: false })
  }

  handleMouseMove = event => {
    const { onMove, id } = this.props
    const xDiff = this.moving.pageX - event.pageX
    const yDiff = this.moving.pageY - event.pageY
    const newX = this.moving.startX - xDiff
    const newY = this.moving.startY - yDiff
    onMove(id, newX, newY)
  }

  // this is necessary, to prevent the parent Map from also reacting to the click event on the WaypointCircle
  handleClick = event => {
    event.stopPropagation()
  }

  waypointLabel() {
    const { index } = this.props
    return (index + 1).toString()
  }

  horizontalLabelOffset() {
    return this.waypointLabel().length * -4.5
  }

  render() {
    const { x, y } = this.props
    const { isMoving } = this.state
    return (
      <Waypoint
        onMouseDown={this.handleMouseDown}
        onMouseUp={this.handleMouseUp}
        onClick={this.handleClick}
        isMoving={isMoving}
      >
        <Circle cx={x} cy={y} />
        <Label x={x} y={y} dy={5} dx={this.horizontalLabelOffset()}>
          {this.waypointLabel()}
        </Label>
      </Waypoint>
    )
  }
}

export default WaypointCircle

const Waypoint = styled.g(props => ({
  cursor: props.isMoving ? 'grabbing' : 'grab'
}))

const Circle = styled.circle(props => ({
  r: CIRCLE_RADIUS,
  fill: props.theme.darkGrey
}))

const Label = styled.text(props => ({
  fontSize: 16,
  fill: props.theme.lightTextColor
}))
