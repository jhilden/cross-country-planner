import React from 'react'
import { storiesOf } from '@storybook/react'
import WaypointCircle from '../WaypointCircle'

const SvgDecorator = storyFn => (
  <svg width={200} height={100} style={{ backgroundColor: 'silver' }}>
    {storyFn()}
  </svg>
)

storiesOf('WaypointCircle', module)
  .addDecorator(SvgDecorator)
  .add('single digit', () => (
    <WaypointCircle index={1} x={50} y={50} onMove={() => {}} />
  ))
  .add('double digit', () => (
    <WaypointCircle index={10} x={50} y={50} onMove={() => {}} />
  ))
  .add('triple digit', () => (
    <WaypointCircle index={100} x={50} y={50} onMove={() => {}} />
  ))
