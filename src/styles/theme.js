// base colors
const COLORS = {
  darkGrey: '#3B3B3B',
  mediumGrey: '#4D4D4D',
  boldMediumGrey: '#737373',
  white: 'white',
  black: 'black',
  green: '#C1E65B',
  blue: '#2481E5'
}

// base colors + aliases
export default {
  ...COLORS,
  lightTextColor: COLORS.white,
  darkTextColor: COLORS.black
}
