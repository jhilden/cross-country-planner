import React from 'react'
import { storiesOf } from '@storybook/react'
import uniqid from 'uniqid'

import CrossCountryPlanner from '../CrossCountryPlanner'

const waypoints = [
  { id: uniqid(), coordinates: { x: 246, y: 198 } },
  { id: uniqid(), coordinates: { x: 478, y: 187 } },
  { id: uniqid(), coordinates: { x: 458, y: 279 } },
  { id: uniqid(), coordinates: { x: 514, y: 300 } },
  { id: uniqid(), coordinates: { x: 567, y: 431 } },
  { id: uniqid(), coordinates: { x: 602, y: 406 } },
  { id: uniqid(), coordinates: { x: 633, y: 448 } },
  { id: uniqid(), coordinates: { x: 631, y: 501 } },
  { id: uniqid(), coordinates: { x: 678, y: 480 } }
]

storiesOf('CrossCountryPlanner', module).add('with sample waypoints', () => (
  <CrossCountryPlanner waypoints={waypoints} />
))
