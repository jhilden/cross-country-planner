import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled/macro'

import WaypointList from './WaypointList'

import gpxDataUrlFor from '../lib/gpxExporter'

class SideBar extends PureComponent {
  static propTypes = {
    waypoints: PropTypes.array.isRequired,
    onWaypointDelete: PropTypes.func.isRequired,
    onWaypointChangeOrder: PropTypes.func.isRequired
  }

  handleDownloadClick = () => {
    const { waypoints } = this.props
    // this generates and triggers a temporary <a> element for the download
    // it's better for perfomance to only do this on demand
    // instead of countinually updateding the `href` value on any `waypoints` change
    const tempLink = document.createElement('a')
    tempLink.href = encodeURI(gpxDataUrlFor(waypoints))
    tempLink.setAttribute('download', 'cross-country-planner_route-export.xml')
    document.body.appendChild(tempLink)
    tempLink.click()
  }

  render() {
    const { waypoints, onWaypointDelete, onWaypointChangeOrder } = this.props
    return (
      <Background>
        <Title>Route Builder</Title>

        {waypoints.length === 0 ? (
          <PlanningInstructions>
            Click on the map to start planning your route
          </PlanningInstructions>
        ) : (
          <Fragment>
            <ListContainer>
              <WaypointList
                waypoints={waypoints}
                onWaypointDelete={onWaypointDelete}
                onWaypointChangeOrder={onWaypointChangeOrder}
              />
            </ListContainer>

            <DownloadButton onClick={this.handleDownloadClick}>
              Download your Route
            </DownloadButton>
          </Fragment>
        )}
      </Background>
    )
  }
}

export default SideBar

const Background = styled.div(props => ({
  backgroundColor: props.theme.darkGrey,
  width: '100%',
  height: '100%',
  display: 'flex',
  flexDirection: 'column'
}))

const Title = styled.div(props => ({
  color: props.theme.lightTextColor,
  marginRight: 20,
  marginLeft: 20,
  marginBottom: 30,
  paddingTop: 20,
  paddingBottom: 15,
  borderBottomWidth: 3,
  borderBottomColor: props.theme.mediumGrey,
  borderBottomStyle: 'solid',
  fontSize: 18,
  fontWeight: 'bold'
}))

const ListContainer = styled.div({
  flex: 1,
  overflow: 'auto'
})

const DownloadButton = styled.a(props => ({
  backgroundColor: props.theme.green,
  color: props.theme.darkTextColor,
  borderRadius: 5,
  margin: 20,
  textAlign: 'center',
  padding: 10,
  fontWeight: 'bold',
  display: 'block',
  textDecoration: 'none',
  cursor: 'pointer'
}))

const PlanningInstructions = styled.div(props => ({
  color: props.theme.lightTextColor,
  margin: 20
}))
