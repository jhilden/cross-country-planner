import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled/macro'

import WaypointListItem from './WaypointListItem'

class WaypointList extends PureComponent {
  static propTypes = {
    waypoints: PropTypes.array.isRequired,
    onWaypointDelete: PropTypes.func.isRequired,
    onWaypointChangeOrder: PropTypes.func.isRequired
  }

  renderTitle() {
    const { index } = this.props
    return `Waypoint ${index + 1}`
  }

  handleDragStart = event => {
    this.sourceIndex = event.currentTarget.getAttribute('data-index')
    event.dataTransfer.effectAllowed = 'move'
  }

  handleDragEnd = event => {
    const { onWaypointChangeOrder } = this.props
    onWaypointChangeOrder(this.sourceIndex, this.targetIndex)
  }

  handleDragOver = event => {
    // stores the index of the list item we are just dragging over
    this.targetIndex = event.target
      .closest('[data-index]')
      .getAttribute('data-index')
  }

  render() {
    const { onWaypointDelete, waypoints } = this.props
    return (
      <ListContainer onDragOver={this.handleDragOver}>
        {waypoints.map((waypoint, index) => (
          <li
            key={waypoint.id}
            data-index={index}
            draggable
            onDragStart={this.handleDragStart}
            onDragEnd={this.handleDragEnd}
          >
            <WaypointListItem
              index={index}
              id={waypoint.id}
              onDeleteClick={onWaypointDelete}
            />
          </li>
        ))}
      </ListContainer>
    )
  }
}

export default WaypointList

const ListContainer = styled.ul({
  margin: 0,
  padding: 0,
  listStyle: 'none'
})
