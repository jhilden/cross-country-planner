import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import styled from '@emotion/styled/macro'

class WaypointListItem extends PureComponent {
  static propTypes = {
    index: PropTypes.number.isRequired,
    onDeleteClick: PropTypes.func.isRequired
  }

  renderTitle() {
    const { index } = this.props
    return `Waypoint ${index + 1}`
  }

  handleDeleteClick = () => {
    const { onDeleteClick, id } = this.props
    onDeleteClick(id)
  }

  render() {
    return (
      <Container>
        <DragHandle>☰</DragHandle>
        <Label>{this.renderTitle()}</Label>
        <DeleteIcon onClick={this.handleDeleteClick}>🗑</DeleteIcon>
      </Container>
    )
  }
}

export default WaypointListItem

const Container = styled.div({
  display: 'flex',
  flexDirection: 'row',
  cursor: 'pointer'
})
const DragHandle = styled.div(props => ({
  color: props.theme.boldMediumGrey,
  padding: 10
}))
const Label = styled.div(props => ({
  color: props.theme.lightTextColor,
  flex: 1,
  padding: 10
}))
const DeleteIcon = styled.div(props => ({
  padding: 10,
  color: props.theme.boldMediumGrey
}))
