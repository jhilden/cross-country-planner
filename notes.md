# Initial Situation

Confident about:

- tooling
- general idea on how to structure the app and the involved state

Uncricital uncertainties:

- GPX format
- how to allow file download of client-side state

Bigger uncertainties:

- Drag & Drop functionality (never implemented without React and without 3rd party libraries)
- Unsure about how to implement the coordinate system of the map
  - Should I use just pixels or real geocoordinates (GPX suggests real coordinates)
  - If I use real coordinates, how to translate between pixels and coordinates?
  - Or wouldn't it make more sense to just rely on Leaflet for the map, even for this coding challenge?

# What I learned on the way

- on the way I figured out that SVG is probably the better technology to draw the route on top of the map, since it is quite difficult to draw edges between coordinates with just HTML/CSS.

# TODOS

- [x] improve WaypointCircles text (centering, and make dragable)
- [x] add storybook
- [ ] more responsive/fluid design
- [ ] better visual drag&drop feedback
- [ ] tests
