import React from 'react'
import { configure, addDecorator } from '@storybook/react'
import { ThemeProvider } from 'emotion-theming'

import '../src/styles/index.css'
import theme from '../src/styles/theme'

function loadStories() {
  require('../src/stories/CrossCountryPlanner.stories.js'),
    require('../src/map/stories/WaypointCircle.stories.js')
}

addDecorator(story => <ThemeProvider theme={theme}>{story()}</ThemeProvider>)

configure(loadStories, module)
